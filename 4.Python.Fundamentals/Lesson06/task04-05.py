'''
4.Реализуйте базовый класс Car.
 У данного класса должны быть следующие атрибуты: speed, color, name, is_police (булево).
 А также методы: go, stop, turn(direction), которые должны сообщать, что машина поехала, остановилась, повернула (куда).
 Опишите несколько дочерних классов: TownCar, SportCar, WorkCar, PoliceCar.
 Добавьте в базовый класс метод show_speed, который должен показывать текущую скорость автомобиля.
 Для классов TownCar и WorkCar переопределите метод show_speed.
 При значении скорости свыше 60 (TownCar) и 40 (WorkCar) должно выводиться сообщение о превышении скорости.

5. Создайте экземпляры классов, передайте значения атрибутов.
 Выполните доступ к атрибутам, выведите результат.
 Выполните вызов методов и также покажите результат.
'''


class Car:
    speed: int = 0
    color: str = None
    name: str = None
    is_police: bool = False

    def __init__(self, speed, color, name, is_police=False):
        self.speed = speed
        self.color = color
        self.name = name
        self.is_police = is_police

    def go(self):
        return f'{self.name} is started'

    def stop(self):
        return f'{self.name} is stopped'

    def turn(self, direction: str):
        return f"The car turned to the {direction}"

    def show_speed(self):
        return f'Current speed {self.name} is {self.speed}'


class TownCar(Car):

    def show_speed(self):
        print(f'Current speed of town car {self.name} is {self.speed}')

        if self.speed > 40:
            return f'Speed of {self.name} is higher than allowed for the town car'
        else:
            return f'Speed of {self.name} is normal for town car'


class WorkCar(Car):
    def __init__(self, speed, color, name, is_police):
        super().__init__(speed, color, name, is_police)

    def show_speed(self):
        print(f'Current speed of work car {self.name} is {self.speed}')

        if self.speed > 60:
            return f'Speed of {self.name} is higher than allowed for the work car'


class PoliceCar(Car):
    def __init__(self, speed, color, name, is_police):
        super().__init__(speed, color, name, is_police)

    def police(self):
        if self.is_police:
            return f'{self.name} is from police department'
        else:
            return f'{self.name} is not from police department'


class SportCar(Car):
    def __init__(self, speed, color, name, is_police):
        super().__init__(speed, color, name, is_police)


sport_car = SportCar(200, 'Yellow', 'Corvette', False)
town_car = TownCar(29, 'Green', 'Hyundai Solaris', False)
work_car = WorkCar(66, 'White', 'Lada Largus', False)
police_car = PoliceCar(120, 'Black', 'Ford', True)
print(sport_car.show_speed())
print(work_car.show_speed())
print(f'Is {town_car.name} a police car? {town_car.is_police}')
print(f'Is {work_car.name}  a police car? {work_car.is_police}')
print(police_car.name, police_car.color, police_car.speed, police_car.is_police)
print(police_car.go(), police_car.turn('LEFT'), police_car.stop())


