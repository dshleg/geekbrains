'''
Реализовать базовый класс Worker (работник), в котором определить атрибуты: name, surname, position (должность), income (доход).
Последний атрибут должен быть защищенным и ссылаться на словарь, содержащий элементы: оклад и премия, например, {"wage": wage, "bonus": bonus}.
Создать класс Position (должность) на базе класса Worker.
В классе Position реализовать методы получения полного имени сотрудника (get_full_name) и дохода с учетом премии (get_total_income).
Проверить работу примера на реальных данных (создать экземпляры класса Position, передать данные, проверить значения атрибутов, вызвать методы экземпляров).
'''

class Worker:

    def __init__(self, name: str, surname: str, position: str, wage, bonus):
        self.firstname: str = name
        self.lastname: str = surname
        self.position: str = position
        self._income = {"wage": float(wage), "bonus": float(bonus)}


class Position(Worker):

    def get_full_name(self):
        return self.firstname + ' ' + self.lastname

    def get_total_income(self):
        return sum(self._income.values())

print("Please enter employee data.")
employee = Position(input("Enter name: "), input("Enter lastname: "), input("Enter position: "),
                    input("Enter wage: "), input("Enter bonus: "))

print(f"Employee with name: {employee.get_full_name()} works on position {employee.position} with salary: {employee.get_total_income()}")
