'''
Реализовать скрипт, в котором должна быть предусмотрена функция расчета заработной платы сотрудника.
В расчете необходимо использовать формулу: (выработка в часах*ставка в час) + премия.
Для выполнения расчета для конкретных значений необходимо запускать скрипт с параметрами.
'''

from sys import argv
calculated_salary = None

try:
    file, working_hours, hour_price, benefit = argv
    calculated_salary = (int(working_hours) * int(hour_price)) + int(benefit)
except ValueError:
    print("Incorrect argv value, or not all args defined")

print(f"Your salary is: {calculated_salary}")