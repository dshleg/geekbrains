'''
Реализовать формирование списка, используя функцию range() и возможности генератора.
В список должны войти четные числа от 100 до 1000 (включая границы).
Необходимо получить результат вычисления произведения всех элементов списка.
'''

from functools import reduce

init_list = [el for el in range(99, 1001) if el % 2 == 0]
print(f"Even list: {init_list}")
print(f"Multiplied list: {reduce(lambda x, y: x * y, init_list)}")
