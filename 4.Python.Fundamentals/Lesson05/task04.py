'''
Создать (не программно) текстовый файл со следующим содержимым:
One — 1
Two — 2
Three — 3
Four — 4

Необходимо написать программу, открывающую файл на чтение и считывающую построчно данные.
При этом английские числительные должны заменяться на русские.
Новый блок строк должен записываться в новый текстовый файл.
'''

rus = {"One": 'Один', "Two": 'Два', "Three": 'Три', "Four": 'Четыре'}
new_dict = []
with open('file04.txt') as fh:
    fd = fh.read().splitlines()
    for line in fd:
        key, value = list(map(str.strip, line.split('—')))
        new_dict.append(rus[key] + " — " + value)

with open('file04_translated.txt', 'w', encoding='utf-8') as fhw:
    fhw.writelines("\n".join(new_dict))



