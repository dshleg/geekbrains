'''
Создать вручную и заполнить несколькими строками текстовый файл, в котором каждая строка должна содержать данные о фирме: название, форма собственности, выручка, издержки.
Пример строки файла: firm_1 ООО 10000 5000.

Необходимо построчно прочитать файл, вычислить прибыль каждой компании, а также среднюю прибыль.
Если фирма получила убытки, в расчет средней прибыли ее не включать.
Далее реализовать список. Он должен содержать словарь с фирмами и их прибылями, а также словарь со средней прибылью.
Если фирма получила убытки, также добавить ее в словарь (со значением убытков).
Пример списка: [{“firm_1”: 5000, “firm_2”: 3000, “firm_3”: 1000}, {“average_profit”: 2000}].

Итоговый список сохранить в виде json-объекта в соответствующий файл.
Пример json-объекта:

[{"firm_1": 5000, "firm_2": 3000, "firm_3": 1000}, {"average_profit": 2000}]
'''

import json
company_profit = {}
sum_profit = []
with open('file07.txt', 'r') as fh:
    for line in fh:
        name, orgtype, profit, expenses = line.split()
        company_profit[name] = float(profit) - float(expenses)

        if company_profit[name] >= 0:
            sum_profit.append(company_profit[name])
    avg_profit = sum(sum_profit) / len(sum_profit)

with open('file07.json', 'w+', encoding="utf-8") as fh_json:
    json.dump([company_profit, {'average_profit': round(avg_profit, 2)}], fh_json)
    fh_json.seek(0)
    print(f"Generated json from file: {fh_json.read()}")