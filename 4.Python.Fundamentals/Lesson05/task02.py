'''
Создать текстовый файл (не программно), сохранить в нем несколько строк,
выполнить подсчет количества строк, количества слов в каждой строке.
'''

with open('file02.txt') as fh:
    print(f"File has number of lines: {len(fh.readlines())}")

    fh.seek(0)
    for idx, line in enumerate(fh, 1):
        print(f"Line number {idx} has {len(line.split())} words.")
