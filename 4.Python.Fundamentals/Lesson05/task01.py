'''
Создать программно файл в текстовом формате, записать в него построчно данные, вводимые пользователем.
Об окончании ввода данных свидетельствует пустая строка.
'''

filename = "file01.txt"

with open(filename, 'a+') as fhandle:
    while True:
        text = input("Enter some text. For stop send empty line :").strip()
        if not text:
            break
        fhandle.writelines(f"{text}\n")
        fhandle.seek(0)

    print(fhandle.read().splitlines())
