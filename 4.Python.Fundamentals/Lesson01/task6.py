# 6. Спортсмен занимается ежедневными пробежками. В первый день его результат составил a километров.
# Каждый день спортсмен увеличивал результат на 10 % относительно предыдущего.
# Требуется определить номер дня, на который результат спортсмена составит не менее b километров.
# Программа должна принимать значения параметров a и b и выводить одно натуральное число — номер дня.

first_result = float(input("Enter first result:"))
end_result = float(input("Enter end result:"))
day_number = 1
while end_result > first_result:
    first_result += first_result / 10
    day_number += 1
if first_result > end_result:
    print(day_number)