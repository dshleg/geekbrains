# Пользователь вводит время в секундах.
# Переведите время в часы, минуты, секунды и выведите в формате чч:мм:сс. Используйте форматирование строк.

time_in_seconds = int(input("Please enter your time in seconds:"))
hour = time_in_seconds // 3600
min = (time_in_seconds % 3600) // 60
sec = (time_in_seconds % 3600) % 60
print(f"Converted time is: {hour:>02}:{min:>02}:{sec:>02}")

