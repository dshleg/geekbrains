'''
4. Начните работу над проектом «Склад оргтехники». Создайте класс, описывающий склад.
 А также класс «Оргтехника», который будет базовым для классов-наследников.
  Эти классы — конкретные типы оргтехники (принтер, сканер, ксерокс).
  В базовом классе определить параметры, общие для приведенных типов.
  В классах-наследниках реализовать параметры, уникальные для каждого типа оргтехники.

5. Продолжить работу над первым заданием.
 Разработать методы, отвечающие за приём оргтехники на склад и передачу в определенное подразделение компании.
 Для хранения данных о наименовании и количестве единиц оргтехники, а также других данных,
 можно использовать любую подходящую структуру, например словарь.

6. Продолжить работу над вторым заданием.
 Реализуйте механизм валидации вводимых пользователем данных.
 Например, для указания количества принтеров, отправленных на склад, нельзя использовать строковый тип данных.
 Подсказка: постарайтесь по возможности реализовать в проекте «Склад оргтехники» максимум возможностей, изученных на уроках по ООП.
'''


class AppError(Exception):
    def __init__(self, text):
        self.text = text

    def __str__(self):
        return self.text


class StorageNotEnoughSpaceError(AppError):
    def __init__(self, text):
        self.text = f"Storage not have space for place your unit:\n {text}"

class StoragePlaceUnitTypeError(AppError):
    def __init__(self):
        self.text = f"Unable to place unit, wrong type given"


class OfficeEquipment:
    brand: str
    model: str
    price: float
    sheets_per_min: int

    def __init__(self, brand: str, model: str, price: float, sheets: int):
        self.brand = brand
        self.model = model
        self.price = price
        self.sheets_per_min = sheets

    def __str__(self):
        return f'{self.brand} {self.model}'


class Printer(OfficeEquipment):
    both_side_printing: bool

    def __init__(self, both_side: bool = False, *args, **kwargs):
        self.both_side_printing = both_side
        super().__init__(*args, **kwargs)


class Scanner(OfficeEquipment):
    auto_feeder: bool

    def __init__(self, auto_feeder: bool = False, *args, **kwargs):
        self.auto_feeder = auto_feeder
        super().__init__(*args, **kwargs)


class MFU(OfficeEquipment):
    auto_feeder: bool
    both_side_printing: bool

    def __init__(self, both_side: bool = False, auto_feeder: bool = False, *args, **kwargs):
        self.auto_feeder = auto_feeder
        self.both_side_printing = both_side
        super().__init__(*args, **kwargs)


class Storage:
    storage_scope: int = 5
    stored: dict = {}

    @classmethod
    def place(cls, unit):
        stored_len = len(Storage.stored)
        if isinstance(unit, OfficeEquipment):
           if stored_len < Storage.storage_scope:
               Storage.stored[stored_len] = unit
           else:
               raise StorageNotEnoughSpaceError(f" Used: {cls.storage_scope} of {len(cls.stored)}")
        else:
            raise StoragePlaceUnitTypeError

    @classmethod
    def pickupbyid(cls, unit_idx: int):
        try:
           return cls.stored.pop(unit_idx)
        except KeyError:
            print(f"Unit with id {unit_idx} not found")


    @classmethod
    def move(cls, unit_idx, dst: 'Department'):
        unit = cls.pickupbyid(unit_idx)
        dst.units[len(dst.units)] = unit

    @classmethod
    def show_units(cls):
        for k, v in cls.stored.items():

            print(f"Unit, idx: {k} data: {cls.stored[k]}")


class Department:
    name: str
    units: dict = {}

    def __init__(self, name: str):
        self.name = name


class Reception(Department):
    units: dict = {}


class LearnClass(Department):
    units: dict = {}


printer = Printer(brand='HP', model='LaserJet 2500', price='1500.34', sheets=30, both_side=True)
scanner = Scanner(brand='Canon', model='Ultascan 3000', price='1000.00', sheets=20)
mfu = MFU(brand='KonicaMinolta', model='Bizhub 224', price='3300.49', sheets=50, both_side=True, auto_feeder=True)


reception = Reception("Reception")
learn_class = LearnClass("Learn Class")

Storage.place(printer)
Storage.place(scanner)
Storage.place(mfu)

print("Storage has some units:")
Storage.show_units()
Storage.move(1, reception)
print(f"\nReception units: {reception.units[0]}")
Storage.move(0, learn_class)

print(f"\nLearn Class units: {learn_class.units[0]}")

print("\nStorage has some units after move:")
Storage.show_units()
