'''
Реализовать класс Matrix (матрица). Обеспечить перегрузку конструктора класса (метод init()),
который должен принимать данные (список списков) для формирования матрицы.
Подсказка: матрица — система некоторых математических величин, расположенных в виде прямоугольной схемы.
Примеры матриц: см. в методичке.

Следующий шаг — реализовать перегрузку метода str() для вывода матрицы в привычном виде.
Далее реализовать перегрузку метода add() для реализации операции сложения двух объектов класса Matrix (двух матриц).
Результатом сложения должна быть новая матрица.
Подсказка: сложение элементов матриц выполнять поэлементно — первый элемент первой строки первой матрицы
складываем с первым элементом первой строки второй матрицы и т.
'''
from itertools import zip_longest


class Matrix:

    def __init__(self, lists: list):
        self.lists = lists

    def __str__(self):
        #print(self.lists)
        return str('\n'.join(['\t'.join([str(j) for j in i]) for i in self.lists]))

    def __add__(self, other):
        return Matrix(list([sum(x) for x in zip(*y)] for y in zip_longest(self.lists, other.lists, fillvalue=0)))





x = Matrix([[1, 4, 7],
            [2, 5, 8],
            [3, 6, 9]])

y = Matrix([[9, 6, 3],
            [8, 5, 2],
            [7, 4, 1]])

print(x + y)
