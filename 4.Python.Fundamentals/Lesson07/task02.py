'''
Реализовать проект расчета суммарного расхода ткани на производство одежды.
Основная сущность (класс) этого проекта — одежда, которая может иметь определенное название.
К типам одежды в этом проекте относятся пальто и костюм.
У этих типов одежды существуют параметры: размер (для пальто) и рост (для костюма).
Это могут быть обычные числа: V и H, соответственно.
Для определения расхода ткани по каждому типу одежды использовать формулы: для пальто (V/6.5 + 0.5), для костюма (2*H + 0.3).
Проверить работу этих методов на реальных данных.
Реализовать общий подсчет расхода ткани.
Проверить на практике полученные на этом уроке знания: реализовать абстрактные классы для основных классов проекта,
проверить на практике работу декоратора @property.
'''
from abc import ABC, abstractmethod


class Dress:
    _height: float
    _size: float
    total: float = 0

    def __init__(self, size: float, height: float):
        self._height = height
        self._size = size

    @abstractmethod
    def material(self):
        pass


class Coat(Dress):
    def __init__(self, size, height=0):
        super().__init__(size, height)

    @property
    def material(self):
        self.total = round(self._size / 6.5 + 0.5, 2)
        Dress.total += self.total
        return self.total


class Jacket(Dress):
    def __init__(self, height, size=0):
        super().__init__(size, height)

    def material(self):
        self.total = round(self._height * 2 + 0.3, 2)
        Dress.total += self.total
        return self.total

coat = Coat(19)
jacket = Jacket(22)
print(f"Total material for Coat: {coat.material}")
print(f"Total material for Jacket: {jacket.material()}")
print(f"Total material for Jacket and Coat: {Dress.total}")
