"""
Создать список и заполнить его элементами различных типов данных.
Реализовать скрипт проверки типа данных каждого элемента. Использовать функцию type() для проверки типа.
Элементы списка можно не запрашивать у пользователя, а указать явно, в программе.
"""
data_int = 10
data_str = "Text"
data_float = 3.14
data_bool = False
data_list = ['x', '1']
data_tuple = ('y', '2')
data_dict = {'name': 'User', 'age': '33'}

new_list = [data_int, data_str, data_float, data_bool, data_list, data_tuple, data_dict]
for element in new_list:
    print(f"Element {element}  has type {type(element)}")
