"""
Реализовать структуру «Рейтинг», представляющую собой не возрастающий набор натуральных чисел.
У пользователя необходимо запрашивать новый элемент рейтинга.
Если в рейтинге существуют элементы с одинаковыми значениями, то новый элемент с тем же значением должен разместиться после них.

Подсказка. Например, набор натуральных чисел: 7, 5, 3, 3, 2.
Пользователь ввел число 3. Результат: 7, 5, 3, 3, 3, 2.
Пользователь ввел число 8. Результат: 8, 7, 5, 3, 3, 2.
Пользователь ввел число 1. Результат: 7, 5, 3, 3, 2, 1.

Набор натуральных чисел можно задать непосредственно в коде, например, my_list = [7, 5, 3, 3, 2].
"""

my_list = [7, 5, 3, 3, 2]
userinput = input("Please enter some digit value: ")

if int(userinput) in my_list:

    stringed_list = [str(int) for int in my_list]
    occurrence_index = "".join(stringed_list).find(userinput)
    my_list.insert(occurrence_index + 1, int(userinput))

else:
    if int(userinput) < min(my_list):
       my_list.append(int(userinput))
    else:
       my_list.insert(0, int(userinput))

print(my_list)
