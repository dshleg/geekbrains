"""
Реализовать функцию, принимающую два числа (позиционные аргументы) и выполняющую их деление.
Числа запрашивать у пользователя, предусмотреть обработку ситуации деления на ноль.
"""


def division(x, y):
    try:
        return x / y


    except ZeroDivisionError:
        return "Incorrect divider"
    except TypeError:
        return "Only digits allowed"

try:
   x = int(input("Enter dividend: "))
   y = int(input("Enter divider: "))
except ValueError:
    print("Only digits allowed")

print(division(x, y))
