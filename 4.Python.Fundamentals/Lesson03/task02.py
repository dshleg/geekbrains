"""
Реализовать функцию, принимающую несколько параметров,
описывающих данные пользователя: имя, фамилия, год рождения, город проживания, email, телефон.
Функция должна принимать параметры как именованные аргументы.
Реализовать вывод данных о пользователе одной строкой.
"""

def user_data(name, lastname, age, email, phone):
    return f"User data: {name} {lastname} {age} {email} {phone}"

print("Please enter user data:")
print(user_data(name=input("Name: "), lastname=input("LastName: "), age=input("Age: "),
                email=input("Mailbox: "), phone=input("Phone Number: ")))
